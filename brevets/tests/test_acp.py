import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from acp_times import *
import arrow
import nose

test_time = arrow.get("2021-11-06 12:00", "YYYY-MM-DD HH:mm")

def test_60():
    
    start = arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")
    start = start.shift(hours=1, minutes=46)
    close = arrow.get("2020-01-01 04:00", "YYYY-MM-DD HH:mm")
    assert (start.isoformat() == open_time(60, 200, arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")))
    assert (close.isoformat() == close_time(60, 200, arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")))
   
    
def test_100():
    
    start = arrow.get("2021-11-06 14:56", "YYYY-MM-DD HH:mm")
    close = arrow.get("2021-11-06 18:40", "YYYY-MM-DD HH:mm")
    assert (start.isoformat() == open_time(100, 200, test_time))
    assert close.isoformat() == close_time(100, 200, test_time)
    

def test_400():
    
    start = arrow.get("2021-11-07 00:08", "YYYY-MM-DD HH:mm")
    close = arrow.get("2021-11-07 14:40", "YYYY-MM-DD HH:mm")
    assert (start.isoformat() == open_time(400, 200, test_time))
    assert close.isoformat() == close_time(400, 200, test_time)


def test_700():

    start = arrow.get("2021-11-07 10:22", "YYYY-MM-DD HH:mm")
    close = arrow.get("2021-11-08 12:45", "YYYY-MM-DD HH:mm")
    assert (start.isoformat() == open_time(700, 200, test_time))
    assert close.isoformat() == close_time(700, 200, test_time)
    
    
def test_1000():
    
    start = arrow.get("2021-11-07 21:05", "YYYY-MM-DD HH:mm")
    close = arrow.get("2021-11-09 15:00", "YYYY-MM-DD HH:mm")
    assert (start.isoformat() == open_time(1000, 200, test_time))
    assert close.isoformat() == close_time(1000, 200, test_time)
    
def new_test_32():
    start = arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")
    open = arrow.get("2020-01-01 00:56", "YYYY-MM-DD HH:mm")  
    close = arrow.get("2020-01-01 02:08", "YYYY-MM-DD HH:mm")
    # print(f"Start {close}, open {close_time(32, 200, "2020-01-01 00:00")}")
    assert (open.isoformat() == open_time(32, 200, "2020-01-01 00:00"))
    assert(close.isoformat() == close_time(32, 200, "2020-01-01 00:00"))
    
    
def new_test_60():
    start = arrow.get("2020-01-01 00:00", "YYYY-MM-DD HH:mm")
    open = arrow.get("2020-01-01 01:46", "YYYY-MM-DD HH:mm")  
    close = arrow.get("2020-01-01 04:00", "YYYY-MM-DD HH:mm")
    assert (open.isoformat() == open_time(60, 200, "2020-01-01 00:00"))
    assert(close.isoformat() == close_time(60, 200, "2020-01-01 00:00"))
    
new_test_60()