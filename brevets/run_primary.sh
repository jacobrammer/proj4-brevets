# docker kill $(docker ps -q) # kill all docker containers, free ports
docker kill PROJ4
docker rm $(docker ps --filter status=exited -q)
# docker build -t proj3:latest 
docker build -t proj4:latest --target=primary .
docker run -i --name PROJ4 -d -p 5000:5000 proj4
