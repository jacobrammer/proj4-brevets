# docker kill $(docker ps -q)  # kill all docker containers, free ports
docker kill PROJ4
docker rm $(docker ps --filter status=exited -q)  # remove exited containers
docker build -t proj4:latest --target=debug .
docker run -i --name PROJ4 -d -p 5000:5000 -p 5678:5678 proj4
